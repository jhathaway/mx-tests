#!/usr/bin/env bats

# Internal tests
#
# *NOTE:* for prod WMF_INT_HOST=sretest1001 must be supplied, for dev, run the
# entire script on an internal test host, so as to use the test dns server

set -o nounset

function setup {
	load 'test_helper/common-setup'
	_common_setup
}

@test 'DKIM #1 Wiki mail (From: wiki@wikimedia.org) is signed with the wiki-mail key' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local server=$WMF_MX
	local rcpt=$MTA_EXT_EMAIL
	# To be signed with the wiki-mail dkim key an email must meet two conditions:
	# 1. SMTP connect must made be to ip 208.80.154.91
	# 2. Email must have header: X-Mailer: MediaWiki mailer
	# Use WMF_INT_HOST so we are not blocked talking to imaps
	if [[ -v WMF_INT_HOST ]]; then
		dkim_cmd=('ssh' "$WMF_INT_HOST")
	else
		dkim_cmd=('bash')
	fi
	"${dkim_cmd[@]}" <<-EOF
		swaks --from 'wiki@wikimedia.org' \
				--to '${rcpt}' \
				--server '${server}' \
				--header 'Subject: ${BATS_TEST_DESCRIPTION}' \
				--body '${BATS_TEST_DESCRIPTION}' \
				--header 'X-Mailer: MediaWiki mailer' \
				--header 'Message-Id: ${msg_id}'
	EOF
	msg=$(mktemp)
	poll-imap-search "$MTA_EXT_EMAIL" "$MTA_EXT_PASS" "HEADER Message-Id ${msg_id}" >"$msg"
	cat "$msg"
	grep ^DKIM-Signature: "$msg"
	grep 's=wiki-mail;' "$msg"
	# https://github.com/emersion/go-msgauth
	! dkim-verify <"$msg" 2>&1 | grep Invalid
	local pstatus=("${PIPESTATUS[@]}")
	rm "$msg"
	[[ ${pstatus[0]} -eq 0 ]] && [[ ${pstatus[1]} -eq 1 ]]
}

@test 'DKIM #2 Non-wiki mail is signed with the wikimedia key' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local rcpt=$MTA_EXT_EMAIL
	local server=$WMF_MX
	# Use WMF_INT_HOST so we are not blocked talking to imaps
	if [[ -v WMF_INT_HOST ]]; then
		dkim_cmd=('ssh' "$WMF_INT_HOST")
	else
		dkim_cmd=('bash')
	fi
	"${dkim_cmd[@]}" <<-EOF
		swaks --from 'jhathaway@wikimedia.org' \
			--to '${rcpt}' \
			--server '${server}' \
			--header 'Subject: ${BATS_TEST_DESCRIPTION}' \
			--body '${BATS_TEST_DESCRIPTION}' \
			--header 'Message-Id: ${msg_id}'
	EOF
	msg=$(mktemp)
	poll-imap-search "$MTA_EXT_EMAIL" "$MTA_EXT_PASS" "HEADER Message-Id ${msg_id}" >"$msg"
	cat "$msg"
	grep ^DKIM-Signature: "$msg"
	grep 's=wikimedia;' "$msg"
	# https://github.com/emersion/go-msgauth
	! dkim-verify <"$msg" 2>&1 | grep Invalid
	local pstatus=("${PIPESTATUS[@]}")
	rm "$msg"
	[[ ${pstatus[0]} -eq 0 ]] && [[ ${pstatus[1]} -eq 1 ]]
}

@test 'DKIM #3 Non-wiki mail from subdomains is signed with the wikimedia key' {
	local rcpt=$MTA_EXT_EMAIL
	local server=$WMF_MX
	# Use WMF_INT_HOST so we are not blocked talking to imaps
	if [[ -v WMF_INT_HOST ]]; then
		dkim_cmd=('ssh' "$WMF_INT_HOST")
	else
		dkim_cmd=('bash')
	fi
	domains=(
		'gitlab.wikimedia.org'
		'phabricator.wikimedia.org'
	)
	for domain in "${domains[@]}"; do
		local msg_id="<TS_${EPOCHREALTIME}@example.com>"
		"${dkim_cmd[@]}" <<-EOF
			swaks --from 'butter@${domain}' \
				--to '${rcpt}' \
				--server '${server}' \
				--header 'Subject: ${BATS_TEST_DESCRIPTION} ${domain}' \
				--body '${BATS_TEST_DESCRIPTION} ${domain}' \
				--header 'Message-Id: ${msg_id}'
		EOF
		msg=$(mktemp)
		poll-imap-search "$MTA_EXT_EMAIL" "$MTA_EXT_PASS" "HEADER Message-Id ${msg_id}" >"$msg"
		cat "$msg"
		grep ^DKIM-Signature: "$msg"
		grep 's=wikimedia;' "$msg"
		# https://github.com/emersion/go-msgauth
		! dkim-verify <"$msg" 2>&1 | grep Invalid
		local pstatus=("${PIPESTATUS[@]}")
		rm "$msg"
		[[ ${pstatus[0]} -eq 0 ]] && [[ ${pstatus[1]} -eq 1 ]]
	done
}
