#!/usr/bin/env bats

set -o nounset

setup() {
	load 'test_helper/common-setup'
	_common_setup
}

@test 'Route #1 Recipient Wikimedia Foundation to Gmail' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local from=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local local_part=$WMF_USER
	local rcpt="${local_part}@wikimedia.org"
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs "$queue_id" 'aspmx.l.google.com' '<jhathaway@wikimedia.org>'
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' "HEADER Message-Id ${msg_id}" >output/"$msg_id"
}

@test 'Route #2 Recipient VRT Contacts to VRTS' {
	local local_part='info'
	local server=$WMF_MX
	local from=$MTA_EXT_EMAIL
	domains=(
		'wikibooks.org'
		'wikidata.org'
		'wikimedia.org'
		'wikinews.org'
		'wikipedia.org'
		'wikiquote.org'
		'wikisource.org'
		'wikiversity.org'
		'wikivoyage.org'
		'wiktionary.org'
	)
	for domain in "${domains[@]}"; do
		local msg_id="<TS_${EPOCHREALTIME}@example.com>"
		local rcpt="${local_part}@${domain}"
		swaks_out=$(mktemp)
		swaks --to "$rcpt" \
			--from "$from" \
			--server "$server" \
			--body "${BATS_TEST_DESCRIPTION}" \
			--header "Message-Id: ${msg_id}" \
			--header "Subject: ${BATS_TEST_DESCRIPTION}" |
			tee /dev/stderr >"$swaks_out"
		read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
		rm "$swaks_out"
		poll-postfix-logs "$queue_id" 'vrts1001.eqiad.wmnet' "$rcpt"
	done
}

@test 'Route #3 Recipient Wiki Operators to Gmail' {
	local local_part='postmaster'
	local server=$WMF_MX
	domains=(
		'wikibooks.org'
		'wikidata.org'
		'wikimedia.org'
		'wikinews.org'
		'wikipedia.org'
		'wikiquote.org'
		'wikisource.org'
		'wikispecies.org'
		'wikiversity.org'
		'wikivoyage.org'
		'wiktionary.org'
	)
	for domain in "${domains[@]}"; do
		local msg_id="<TS_${EPOCHREALTIME}@example.com>"
		local rcpt="${local_part}@${domain}"
		swaks_out=$(mktemp)
		swaks --to "$rcpt" \
			--from "$MTA_EXT_EMAIL" \
			--server "$server" \
			--body "${BATS_TEST_DESCRIPTION}" \
			--header "Message-Id: ${msg_id}" \
			--header "Subject: ${BATS_TEST_DESCRIPTION}" |
			tee /dev/stderr >"$swaks_out"
		read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
		rm "$swaks_out"
		poll-postfix-logs "$queue_id" 'aspmx.l.google.com' 'jhathaway@wikimedia.org'
	done
}

@test 'Route #4 Recipient domain donate.wikimedia.org to CiviCRM' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local from=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local rcpt='root@donate.wikimedia.org'
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs "$queue_id" 'civi1001.frack.eqiad.wmnet' "$rcpt"
}

# VERP
# To create a VERP dummy server for testing, run on the destination MTA:
#    nohup bash -c "while true; do printf 'HTTP/1.1 200 OK\n' | nc -Nl 0.0.0.0 8080; done" > /tmp/nc.log &
@test 'VERP #1 Local VERP address generates a POST to https://api-rw.discovery.wmnet/w/api.php' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local from=$MTA_EXT_EMAIL
	local url_enc_msg_id
	url_enc_msg_id=$(jq -sRr @uri <<<$msg_id)
	local local_part='wiki-enwiki-na1sy-rgptrf-pvBuGAz5f2Tso37v'
	local server=$WMF_MX
	local rcpt="${local_part}@wikimedia.org"
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs "$queue_id" 'wiki-verp-bounce-handler' "$rcpt"
	poll ssh "$server" grep "$url_enc_msg_id" "/tmp/nc.log"
}

@test 'Aliases #1 Generic per domain aliases' {
	local local_part='privacy'
	local server=$WMF_MX
	local from=$MTA_EXT_EMAIL
	domains=(
		'mediawiki.org'
		'w.wiki'
		'wikibooks.org'
		'wikidata.org'
		'wikinews.org'
		'wikiquote.org'
		'wikisource.org'
		'wikispecies.org'
		'wikiversity.org'
		'wikivoyage.de'
		'wiktionary.org'
	)
	for domain in "${domains[@]}"; do
		local msg_id="<TS_${EPOCHREALTIME}@example.com>"
		local rcpt="${local_part}@${domain}"
		swaks_out=$(mktemp)
		swaks --to "$rcpt" \
			--from "$from" \
			--server "$server" \
			--body "${BATS_TEST_DESCRIPTION}" \
			--header "Message-Id: ${msg_id}" \
			--header "Subject: ${BATS_TEST_DESCRIPTION}" |
			tee /dev/stderr >"$swaks_out"
		read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
		rm "$swaks_out"
		poll-postfix-logs "$queue_id" 'aspmx.l.google.com' 'privacy@wikimedia.org'
	done
}

@test 'Aliases #2 Specific per domain aliases' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local from=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local rcpt='butter@wikipedia.org'
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs "$queue_id" 'aspmx.l.google.com' "$rcpt"
}
