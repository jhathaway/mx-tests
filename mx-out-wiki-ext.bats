#!/usr/bin/env bats

# External tests, to be run from outside our infra

set -o nounset

function setup {
	load 'test_helper/common-setup'
	_common_setup
}

@test 'Relay #1 Unauthorized relaying for wiki domains is forbidden' {
	local exim='^550'
	local postfix='^554 5.7.1 .*: Relay access denied$'
	local server=$WMF_MX
	local from='postmaster@yahoo.com'
	domains=(
		'wikibooks.org'
		'wikidata.org'
		'wikimedia.org'
		'wikinews.org'
		'wikipedia.org'
		'wikiquote.org'
		'wikisource.org'
		'wikiversity.org'
		'wikivoyage.org'
		'wiktionary.org'
	)
	for domain in "${domains[@]}"; do
		swaks --no-hints \
			--to "postmaster@${domain}" \
			--server "$server" \
			--from "$from" \
			--header "Subject: ${BATS_TEST_DESCRIPTION}" \
			--body "${BATS_TEST_DESCRIPTION}" | tee /dev/stderr |
			cat |
			grep -E "(${exim}|${postfix})"
	done
}

@test 'Relay #2 Unauthorized relaying is forbidden' {
	local exim='^550'
	local postfix='^554 5.7.1 .*: Relay access denied$'
	local server=$WMF_MX
	local from='postmaster@yahoo.com'
	swaks --no-hints \
		--to 'butter@microsoft.com' \
		--server "$server" \
		--from "$from" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" | tee /dev/stderr |
		grep -E "(${exim}|${postfix})"
}

@test 'Relay #3 Authenticated relaying over TLS is allowed' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local rcpt=$MTA_EXT_EMAIL
	local from=$AUTH_RELAY_EMAIL
	local auth_email=$AUTH_RELAY_EMAIL
	local auth_pass=$AUTH_RELAY_PASS
	local server=$WMF_MX
	swaks -tls \
		--to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--auth PLAIN --auth-user "$auth_email" \
		--auth-password "$auth_pass" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}"
	msg=$(mktemp)
	poll-imap-search "$MTA_EXT_EMAIL" "$MTA_EXT_PASS" "HEADER Message-Id ${msg_id}" >output/"$msg_id"
}

@test 'Relay #4 Authenticated relaying over plain TCP is not offered' {
	local rcpt=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local from=$AUTH_RELAY_EMAIL
	local auth_email=$AUTH_RELAY_EMAIL
	! swaks --to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--auth 'PLAIN' \
		--auth-user "$auth_email" \
		--auth-password testpass |
		tee /dev/stderr |
		grep -E '250-AUTH PLAIN'
	local pstatus=("${PIPESTATUS[@]}")
	[[ ${pstatus[0]} -eq 28 ]] && [[ ${pstatus[2]} -eq 1 ]]
}

@test 'Relay #5 Authenticated relaying passwords are checked' {
	local rcpt=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local from=$AUTH_RELAY_EMAIL
	local auth_email=$AUTH_RELAY_EMAIL
	swaks -tls \
		--no-hints \
		--to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--auth 'PLAIN' \
		--auth-user "$auth_email" \
		--auth-password 'badpassword' |
		tee /dev/stderr |
		grep -E '^535'
}

@test 'Relay #6 Authenticated relaying rejects unknown auth users' {
	local rcpt=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local from=$AUTH_RELAY_EMAIL
	local auth_email='does.not.exist@wikimedia.org'
	swaks -tls \
		--no-hints \
		--to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--auth 'PLAIN' \
		--auth-user "$auth_email" \
		--auth-password 'testpass' |
		tee /dev/stderr |
		grep -E '^535'
}

@test 'Relay #7 Authenticated relaying auth user must match email.From address' {
	local exim='^550'
	local postfix='^553 5.7.1 .*: Sender address rejected: not owned by user'
	local rcpt=$MTA_EXT_EMAIL
	local server=$WMF_MX
	local from='mr.president@wikimedia.org'
	local auth_email=$AUTH_RELAY_EMAIL
	local auth_pass=$AUTH_RELAY_PASS
	swaks -tls \
		--no-hints \
		--to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--auth 'PLAIN' \
		--auth-user "$auth_email" \
		--auth-password "$auth_pass" |
		tee /dev/stderr |
		grep -E "(${exim}|${postfix})"
}

@test 'Ingress #1 Local mail is not accepted' {
	local exim='^550'
	local postfix='^504 5.5.2'
	local server=$WMF_MX
	local from='postmaster@yahoo.com'
	swaks --no-hints \
		--to 'root' \
		--server "$server" \
		--from "$from" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" | tee /dev/stderr |
		grep -E "(${exim}|${postfix})"
}
