#!/usr/bin/env bats
#
# How to run:
#   WMF_MX=mta-inbound-wiki WMF_DOMAIN=wikimedia.org ./mx-all.bats

set -o nounset
shopt -s lastpipe

function setup {
	load 'test_helper/common-setup'
	_common_setup
}

@test 'Route #1 Recipient postmaster forwarded to postmaster@wikimedia.org' {
	local local_part='postmaster'
	local server=$WMF_MX
	domains=(
		'wikibooks.org'
		'wikidata.org'
		'wikimedia.org'
		'wikinews.org'
		'wikipedia.org'
		'wikiquote.org'
		'wikisource.org'
		'wikiversity.org'
		'wikivoyage.org'
		'wiktionary.org'
	)
	for domain in "${domains[@]}"; do
		local msg_id="<TS_${EPOCHREALTIME}@example.com>"
		local rcpt="${local_part}@${domain}"
		swaks_out=$(mktemp)
		swaks --to "$rcpt" \
			--from "$MTA_EXT_EMAIL" \
			--server "$server" \
			--body "${BATS_TEST_DESCRIPTION}" \
			--header "Message-Id: ${msg_id}" \
			--header "Subject: ${BATS_TEST_DESCRIPTION}" |
			tee /dev/stderr >"$swaks_out"
		read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
		rm "$swaks_out"
		poll-postfix-logs "$queue_id" 'aspmx.l.google.com' '<jhathaway@wikimedia.org>'
		poll-imap-search 'jhathaway@wikimedia.org' 'butter' "HEADER Message-Id ${msg_id}" >output/"$msg_id"
	done
}

# XXX skip in prod?, or use personal account?
@test 'Spam #1 Headers stripped on incoming mail' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local local_part='postmaster'
	local user='root'
	local rcpt="${local_part}@${domain}"
	swaks --to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--header 'X-Spam-Score: 8.8'
	msg=$(mktemp)
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' \
		"HEADER Message-Id ${msg_id}" >"$msg"
	! grep '^X-Spam-Score: 8.8' "$msg"
	local pstatus=("${PIPESTATUS[@]}")
	[[ ${pstatus[0]} -eq 1 ]]
	rm "$msg"
}

@test 'Spam #2 Postmaster mail, no spam checking' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local local_part='postmaster'
	local rcpt="${local_part}@${domain}"
	swaks --to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--header 'X-Spam-Score: 8.8'
	msg=$(mktemp)
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' \
		"HEADER Message-Id ${msg_id}" >"$msg"
	! grep '^X-Spam-Score:' "$msg"
	local pstatus=("${PIPESTATUS[@]}")
	[[ ${pstatus[0]} -eq 1 ]]
	rm "$msg"
}

@test 'Spam #3 Abuse mail, no spam checking' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local local_part='abuse'
	local rcpt="${local_part}@${domain}"
	swaks --to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--header 'X-Spam-Score: 8.8'
	msg=$(mktemp)
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' \
		"HEADER Message-Id ${msg_id}" >"$msg"
	! grep '^X-Spam-Score:' "$msg"
	local pstatus=("${PIPESTATUS[@]}")
	[[ ${pstatus[0]} -eq 1 ]]
	rm "$msg"
}

@test 'Spam #4 Spam score headers present' {
	# NOTE:
	# Rspamd needs:
	#     enable_test_patterns = true;
	# for this to pass
	local gtube='YJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X'
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local local_part=$WMF_USER
	local rcpt="${local_part}@${domain}"
	swaks --to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}\n${gtube}\n" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}"
	msg=$(mktemp)
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' \
		"HEADER Message-Id ${msg_id}" >"$msg"
	cat "$msg"
	local spamassassin='^X-Spam-Score:'
	local rspamd='^X-Spam:'
	! grep -E "(${spamassassin}|${rspamd})" "$msg"
	local pstatus=("${PIPESTATUS[@]}")
	[[ ${pstatus[0]} -eq 0 ]]
	rm "$msg"
}

@test 'Spam #5 Authentication-Results header present, DKIM checked' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local local_part=$WMF_USER
	local rcpt="${local_part}@${domain}"
	swaks --to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}\n" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}"
	msg=$(mktemp)
	poll-imap-search "$rcpt" 'butter' \
		"HEADER Message-Id ${msg_id}" | tee "$msg"
	cat $msg
	grep -E '^Authentication-Results:' "$msg"
	rm $msg
}

@test 'Spam #6 Discard known spammer addresses' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local local_part='postmaster'
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt="${local_part}@${domain}"
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from 'known-spammer@gmail.com' \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs-regex "$queue_id" 'milter-discard'
}

@test 'Spam #7 Skip spam checking for trusted networks' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local local_part='postmaster'
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt="${local_part}@${domain}"
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--xclient-addr 10.1.1.99 \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-rspamd-logs-regex "$queue_id" 'TRUSTED_NETWORKS'
}

@test 'TLS #1 STARTTLS is supported' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local local_part='postmaster'
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt="${local_part}@${domain}"
	swaks -tls \
		--to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}"
}

@test 'Relay #1 Unauthorized relaying is forbidden' {
	local exim='^550'
	local postfix='^554 5.7.1 .*: Relay access denied$'
	local server=$WMF_MX
	local from='postmaster@yahoo.com'
	swaks --no-hints \
		--to 'butter@microsoft.com' \
		--server "$server" \
		--from "$from" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" | tee /dev/stderr |
		grep -E "(${exim}|${postfix})"
}

@test 'Relay #2 Authenticated relaying over TLS is forbidden' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local rcpt=$MTA_EXT_EMAIL
	local from=$AUTH_RELAY_EMAIL
	local auth_email=$AUTH_RELAY_EMAIL
	local auth_pass=$AUTH_RELAY_PASS
	local server=$WMF_MX
	! swaks -tls \
		--to "$rcpt" \
		--from "$from" \
		--server "$server" \
		--auth PLAIN --auth-user "$auth_email" \
		--quit EHLO \
		--auth-password "$auth_pass" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" | tee /dev/stderr |
		grep -E "(250-AUTH)"
	local pstatus=("${PIPESTATUS[@]}")
	declare -p pstatus
	[[ ${pstatus[0]} -eq 0 ]] && [[ ${pstatus[2]} -eq 1 ]]
}

@test 'Verify #1 Invalid sender domain is rejected' {
	local local_part=$WMF_USER
	local server=$WMF_MX
	swaks --no-hints \
		--from 'bogus@bogus.example.com' \
		--server "$server" \
		--to "${local_part}@${WMF_DOMAIN}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" \
		--body "${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr |
		grep -E \
			'(^550|^450 4.1.8 .*: Sender address rejected: Domain not found)'
}
