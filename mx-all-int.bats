#!/usr/bin/env bats

set -o nounset
shopt -s lastpipe

function setup {
	load 'test_helper/common-setup'
	_common_setup
}

@test 'Local #1 Purist, rewrite headers only in mail from Postfix sendmail and in SMTP mail from this machine' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local local_part='postmaster'
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt='purist'
	swaks_out=$(mktemp)
	swaks --to "$rcpt, jhathaway@wikimedia.org" \
		--from 'wiki@wikimedia.org' \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	msg=$(mktemp)
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' "HEADER Message-Id ${msg_id}" >"$msg"
	cat $msg
	grep 'purist,' $msg
}

@test 'Local #2 Local mail to root via sendmail is supported' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	ssh "$WMF_MX" /sbin/sendmail root <<-EOF
		Message-Id: ${msg_id}
		Subject: ${BATS_TEST_DESCRIPTION}
		To: root

		${BATS_TEST_DESCRIPTION}
	EOF
	poll-imap-search 'jhathaway@wikimedia.org' 'butter' "HEADER Message-Id ${msg_id}"
}

@test 'Local #3 Unqualified, nonexistent recipient mail from mynetworks is rejected' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt='purist'
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from 'wiki@wikimedia.org' \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	grep '550 5.1.1' "$swaks_out"
	rm "$swaks_out"
}

@test 'Local #4 discard mail to wiki@wikimedia.org' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt='wiki@wikimedia.org'
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from 'root@wikimedia.org' \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs "$queue_id" 'none' "<${rcpt}>"
}

@test 'Aliases #1 Local aliases are redirected to root@wikimedia.org' {
	local_parts=(
		'mailer-daemon'
		'postmaster'
		'root'
	)
	for local_part in "${local_parts[@]}"; do
		local msg_id="<TS_${EPOCHREALTIME}@example.com>"
		swaks_out=$(mktemp)
		ssh "$WMF_MX" <<-EOF |
			swaks --to '$local_part' \
				--server 'localhost' \
				--body '${BATS_TEST_DESCRIPTION}' \
				--header 'Message-Id: ${msg_id}' \
				--header 'Subject: ${BATS_TEST_DESCRIPTION}'
		EOF
			tee /dev/stderr >"$swaks_out"
		read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
		rm "$swaks_out"
		poll-imap-search 'jhathaway@wikimedia.org' 'butter' "HEADER Message-Id ${msg_id}"
	done
}
