# MX tests for Wikimedia Foundation email servers

This repository contains a set of black box email tests, which can be
used against production or dev environments. These tests attempt to
assert that the WMF email servers are configured correctly based on
their requirements.

## Running tests

    # Test args are passed as env vars, since bats does not allow passing cli args
    # at present: https://github.com/bats-core/bats-core/issues/357
    $ WMF_DOMAIN=wikimedia.org WMF_MX=mx1001.wikimedia.org ./mx-out-wiki-ext.bats

## Setup a dcl lab

- [ ] Create lab: `make`

- [ ] puppet lab hosts (thrice)

  - [ ] dns-dev1001
  - [ ] mta-dev1001
  - [ ] mx-out1001

- [ ] sretest1001

      (sreteste1001)$ sudo apt install swaks libnet-ssleay-perl bats go-msgauth

- [ ] update resolv.conf on subset of hosts, using dns-dev1001's ip

  - [ ] mx-out1001: `sudo systemctl restart dovecot postfix`
  - [ ] sretest1001
