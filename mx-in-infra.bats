#!/usr/bin/env bats

set -o nounset

setup() {
	load 'test_helper/common-setup'
	_common_setup
}

@test "Infra Route #1 Recipient phabricator domain relayed to phabricator.discovery.wmnet" {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local user='root'
	local server=$WMF_MX
	local rcpt='info@phabricator.wikimedia.org'
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from "$MTA_EXT_EMAIL" \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	read -r _ _ _ _ _ _ queue_id _ < <(grep '250 2.0.0' "$swaks_out")
	rm "$swaks_out"
	poll-postfix-logs "$queue_id" 'phabricator.discovery.wmnet'
	poll-imap-search 'info@phabricator.wikimedia.org' 'butter' "HEADER Message-Id ${msg_id}" >output/"$msg_id"
}
