#!/usr/bin/env bats

source ./config.bats

function _common_setup {
	if [[ $WMF_DOMAIN == 'wikimedia.org' ]]; then
		WMF_USER=$USER
	else
		WMF_USER='postmaster'
	fi
}

# Poll command for 5secs, if successfully return, command must be idempotent
function poll {
	local start_secs=$EPOCHSECONDS
	while (((EPOCHSECONDS - start_secs) < 5)); do
		if "$@" >&2; then
			return 0
		fi
		sleep 0.1
	done
	return 1
}

# Check if an email was delivered successfully by polling IMAP for a single
# email based on the supplied query param for 10secs. If the message is found,
# it is then deleted and success is returned. Otherwise an error is returned.
function poll-imap-search {
	local user=$1
	local passwd=$2
	local query=$3

	local start_secs=$EPOCHSECONDS
	local curl_opts=('--verbose' '--silent' '--max-time' '15')
	while (((EPOCHSECONDS - start_secs) < 15)); do
		if ! resp=$(curl "${curl_opts[@]}" "$MTA_EXT_IMAP" \
			-u "${user}:${passwd}" \
			-X "UID SEARCH ${query}"); then
			local pstatus=("${PIPESTATUS[@]}")
			if [[ ${pstatus[0]} -ne 0 ]]; then
				printf 'Error: curl exited with: %d\n' "${pstatus[0]}" 1>&2
				return 1
			fi
		fi
		IFS=$' \r\n' read -r _ _ uid _ <<<$resp
		if [[ $uid =~ [0-9]+ ]]; then
			# grab email by uid
			if ! curl "${curl_opts[@]}" "${MTA_EXT_IMAP};UID=${uid}" \
				-u "${user}:${passwd}"; then
				printf 'Error: Unable to fetch mail by uid\n' 1>&2
				return 1
			fi
			# delete email by uid
			if ! curl "${curl_opts[@]}" "$MTA_EXT_IMAP" \
				-u "${user}:${passwd}" \
				-X 'UID STORE '"${uid}"' +Flags \Deleted' 1>&2; then
				printf 'Error: Unable to delete mail by uid\n' 1>&2
				return 1
			fi
			return 0
		fi
		sleep 0.1
	done
	printf 'Error: Timeout querying for email: "%s"\n' "$query" 1>&2
	return 1
}

function split {
	# Usage: split "string" "delimiter"
	! IFS=$'\n' read -d '' -ra arr <<<"${1//$2/$'\n'}"
	printf '%s\n' "${arr[@]}"
}

# Use a crude check to see if mail was routed correctly.
#
# args
#   queue_id - postfix queue id
#   relay_host - next hop destnation
#
# Look for postfix log lines of the form, match on queue_id and relay_host
# e.g:
#     2024-06-07T16:10:12.780631+00:00 mx-in1001 postfix/smtp[240062]: 96214CFF5C: to=<info@phabricator.wikimedia.org>, relay=phabricator.discovery.wmnet[10.64.17.71]:25, delay=0.18, delays=0.03/0.02/0.1/0.04, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as BABB8B0BB1)
function poll-postfix-logs {
	local queue_id=$1
	if [[ -z $queue_id ]]; then
		printf 'Error: queue_id is empty\n' 1>&2
		return 1
	fi
	local relay_host=$2
	if [[ -z $relay_host ]]; then
		printf 'Error: relay_host is empty\n' 1>&2
		return 1
	fi
	if [[ $# > 2 ]]; then
		local check_to='true'
		local to=$3
	else
		local check_to='false'
	fi
	local start_secs=$EPOCHSECONDS
	local postfix_log
	while (((EPOCHSECONDS - start_secs) < 10)); do
		if ! postfix_logs=$(
			ssh "$WMF_MX" grep -E "'${queue_id}.* status='" /var/log/postfix.log
		); then
			local pstatus=("${PIPESTATUS[@]}")
			if [[ ${pstatus[0]} -ne 1 ]]; then
				printf 'Error: ssh exited with: %d\n' "${pstatus[0]}" 1>&2
				return 1
			fi
		fi
		if [[ -n "$postfix_logs" ]]; then
			while IFS= read -r postfix_log; do
				postfix_log_stripped=$(sed -E 's/.* '"$queue_id"': //' <<<$postfix_log)
				mapfile -t log_entries < <(split "$postfix_log_stripped" ', ')
				declare -A log_data
				for log_entry in "${log_entries[@]}"; do
					if [[ $log_entry =~ [a-z]+= ]]; then
						IFS='=' read -r k v <<<$log_entry
						log_data["$k"]=$v
					fi
				done
				if [[ ${log_data['dsn']} == '2.0.0' ]] &&
					[[ ${log_data['relay']} =~ ^$relay_host ]]; then
					if [[ $check_to == 'true' ]]; then
						if [[ ${log_data['to']} == "$to" ]]; then
							return 0
						else
							# HACK: keep processing incase more logs match, in the
							# case of aliases
							printf 'Warn: log line did not match, "%s"\n' "${postfix_log}" 1>&2
						fi
					fi
					return 0
				else
					printf 'Error: log line did not match, "%s"\n' "${postfix_log}" 1>&2
					return 1
				fi
			done <<<$postfix_logs
		fi
		sleep 0.1
	done
	printf 'Error: Timeout querying for postfix queue id: "%s"\n' "$queue_id" 1>&2
	return 1
}

function poll-postfix-logs-regex {
	local queue_id=$1
	if [[ -z $queue_id ]]; then
		printf 'Error: queue_id is empty\n' 1>&2
		return 1
	fi
	local regex=$2
	if [[ -z $regex ]]; then
		printf 'Error: regex is empty\n' 1>&2
		return 1
	fi
	local start_secs=$EPOCHSECONDS
	local postfix_log
	while (((EPOCHSECONDS - start_secs) < 10)); do
		if ! postfix_logs=$(
			ssh "$WMF_MX" grep -E "' ${queue_id}: '" /var/log/postfix.log
		); then
			local pstatus=("${PIPESTATUS[@]}")
			if [[ ${pstatus[0]} -ne 1 ]]; then
				printf 'Error: ssh exited with: %d\n' "${pstatus[0]}" 1>&2
				return 1
			fi
		fi
		if [[ -n postfix_logs ]]; then
			while IFS= read -r postfix_log; do
				postfix_log_stripped=$(sed -E 's/.* '"$queue_id"': //' <<<$postfix_log)
				if [[ $postfix_log_stripped =~ $regex ]]; then
					printf 'Log matched, "%s"\n' "${postfix_log}" 1>&2
					return 0
				else
					# HACK: keep processing incase more logs match
					printf 'Warn: log line did not match, "%s"\n' "${postfix_log}" 1>&2
				fi
			done <<<$postfix_logs
		fi
		sleep 0.1
	done
	printf 'Error: Timeout querying for postfix queue id: "%s"\n' "$queue_id" 1>&2
	return 1
}

function poll-rspamd-logs-regex {
	local queue_id=$1
	if [[ -z $queue_id ]]; then
		printf 'Error: queue_id is empty\n' 1>&2
		return 1
	fi
	local regex=$2
	if [[ -z $regex ]]; then
		printf 'Error: regex is empty\n' 1>&2
		return 1
	fi
	local start_secs=$EPOCHSECONDS
	local rspamd_log
	while (((EPOCHSECONDS - start_secs) < 10)); do
		if ! rspamd_logs=$(
			ssh "$WMF_MX" sudo grep -E "'<${queue_id}>'" /var/log/rspamd/rspamd.log
		); then
			local pstatus=("${PIPESTATUS[@]}")
			if [[ ${pstatus[0]} -ne 1 ]]; then
				printf 'Error: ssh exited with: %d\n' "${pstatus[0]}" 1>&2
				return 1
			fi
		fi
		if [[ -n rspamd_logs ]]; then
			while IFS= read -r rspamd_log; do
				if [[ $rspamd_log =~ $regex ]]; then
					printf 'Log matched, "%s"\n' "${rspamd_log}" 1>&2
					return 0
				else
					# HACK: keep processing incase more logs match
					printf 'Warn: log line did not match, "%s"\n' "${rspamd_log}" 1>&2
				fi
			done <<<$rspamd_logs
		fi
		sleep 0.1
	done
	printf 'Error: Timeout querying for rspamd queue id: "%s"\n' "$queue_id" 1>&2
	return 1
}
