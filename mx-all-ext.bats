#!/usr/bin/env bats

# External tests, to be run from outside our infra

set -o nounset

function setup {
	load 'test_helper/common-setup'
	_common_setup
}

@test 'TLS #1 Certificate verifies' {
	local server=$WMF_MX
	openssl s_client -verify_return_error -brief -starttls smtp \
		-connect "${server}:25" </dev/null
}

@test 'Local #1 Remote unqualified recipient mail is rejected' {
	local msg_id="<TS_${EPOCHREALTIME}@example.com>"
	local domain=$WMF_DOMAIN
	local server=$WMF_MX
	local rcpt='purist'
	swaks_out=$(mktemp)
	swaks --to "$rcpt" \
		--from 'wiki@wikimedia.org' \
		--server "$server" \
		--body "${BATS_TEST_DESCRIPTION}" \
		--header "Message-Id: ${msg_id}" \
		--header "Subject: ${BATS_TEST_DESCRIPTION}" |
		tee /dev/stderr >"$swaks_out"
	grep '504 5.5.2' "$swaks_out"
	rm "$swaks_out"
}
