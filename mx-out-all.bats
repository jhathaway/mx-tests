#!/usr/bin/env bats
#
# How to run:
#   WMF_MX=mx-wiki WMF_DOMAIN=wikimedia.org ./mx-all.bats

set -o nounset
shopt -s lastpipe

function setup {
	load 'test_helper/common-setup'
	_common_setup
}
