all: dcl

sync:
	rsync -av *.bats test_helper sretest1001:

dcl:
	dcl lab start
	dcl add nodes -c bookworm dns-dev1001.wikimedia.org
	dcl add nodes -c bookworm mta-dev1001.eqiad.wmnet
	dcl add nodes -c bookworm mx-out1001.wikimedia.org
	dcl add nodes -c bookworm sretest1001.eqiad.wmnet
